//
//  SpeedTestLevel.m
//  Multy
//
//  Created by 吳昌鴻 on 2018/1/10.
//  Copyright © 2018年 Nick. All rights reserved.
//

//#import "SpeedTestLevel.h"
//
//@implementation SpeedTestLevel
//+(UIImage *)getLevelIconWithSpeed:(NSInteger)download{
//    return [self getLevelIcon:[self getSpeedLevel:download]];
//}
//+(UIImage *)getLevelLineWithSpeed:(NSInteger)download{
//    return [self getLevelLine:[self getSpeedLevel:download]];
//}
//+(EnumSpeedLevel)getSpeedLevel:(NSInteger)download{
//    EnumSpeedLevel speedLevel;
//
//    if(download >= 25 && download < 100)
//        speedLevel = EnumSpeedLevel_UltraHD; // 4K
//    else if(download >= 10 && download < 25)
//        speedLevel = EnumSpeedLevel_FHD;
//    else if(download >= 5 && download < 10)
//        speedLevel = EnumSpeedLevel_HD;
//     else if(download >= 100)
//         speedLevel = EnumSpeedLevel_8K;
//    else
//        speedLevel = EnumSpeedLevel_SD;
//    return speedLevel;
//}
//+(UIImage *)getLevelLine:(EnumSpeedLevel)speedLevel
//{
//    UIImage *image;
//    NSString *name;
//    NSString *currentTheme = [UserSetting readNSUserDefaults:ThemeName];
//
//    switch (speedLevel){
//        case EnumSpeedLevel_SD:
//            name = [NSString stringWithFormat:@"%@/e2_img_a_bar_3", currentTheme];
//            break;
//        case EnumSpeedLevel_HD:
//            name = [NSString stringWithFormat:@"%@/e2_img_a_bar_2", currentTheme];
//            break;
//        case EnumSpeedLevel_FHD:
//            name = [NSString stringWithFormat:@"%@/e2_img_a_bar_1", currentTheme];
//            break;
//        case EnumSpeedLevel_UltraHD:
//            name = [NSString stringWithFormat:@"%@/e2_img_a_bar_0", currentTheme];
//            break;
//        case EnumSpeedLevel_8K:
//            name = [NSString stringWithFormat:@"%@/e2_img_a_bar_0", currentTheme];
//            break;
//        default:
//            name = nil;
//            break;
//    }
//    image = [UIImage imageNamed: name];
//    return image;
//}
//+(UIImage *)getLevelIcon:(EnumSpeedLevel)speedLevel{
//    UIImage *image;
//    NSString *name;
//    NSString *currentTheme = [UserSetting readNSUserDefaults:ThemeName];
//    
//    switch (speedLevel){
//        case EnumSpeedLevel_SD:
//            name = [NSString stringWithFormat:@"%@/icon_SD_speedtest", currentTheme];
//            break;
//        case EnumSpeedLevel_HD:
//            name = [NSString stringWithFormat:@"%@/icon_HD_speedtest", currentTheme];
//            break;
//        case EnumSpeedLevel_FHD:
//            name = [NSString stringWithFormat:@"%@/icon_FHD_speedtest", currentTheme];
//            break;
//        case EnumSpeedLevel_UltraHD:
//            name = [NSString stringWithFormat:@"%@/icon_4K_speedtest", currentTheme];
//            break;
//        case EnumSpeedLevel_8K:
//            name = [NSString stringWithFormat:@"%@/icon_8k_speedtest", currentTheme];
//            break;
//        default:
//            name = nil;
//            break;
//    }
//    image = [UIImage imageNamed: name];
//    return image;
//}
//@end
