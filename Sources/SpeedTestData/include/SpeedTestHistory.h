//
//  SpeedTestHistory.h
//  Multy
//
//  Created by 吳昌鴻 on 2018/1/9.
//  Copyright © 2018年 Nick. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface SpeedTestRecord : NSObject
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *time;
@property (nonatomic, strong) NSString *downloadSpeed;
@property (nonatomic, strong) NSString *uploadSpeed;
@end

@interface SpeedTestHistory : NSObject
+(void)delAllRecords;
+(NSMutableArray *)getAllRecords;
+(void)setAllRecords:(NSArray *)records;
+(void)addRecord:(SpeedTestRecord *)record;
@end
