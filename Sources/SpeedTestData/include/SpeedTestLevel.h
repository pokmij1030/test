//
//  SpeedTestLevel.h
//  Multy
//
//  Created by 吳昌鴻 on 2018/1/10.
//  Copyright © 2018年 Nick. All rights reserved.
//
//
//#import <Foundation/Foundation.h>
//
//typedef NS_ENUM(NSUInteger, EnumSpeedLevel) {
//    EnumSpeedLevel_SD,
//    EnumSpeedLevel_HD,
//    EnumSpeedLevel_FHD,
//    EnumSpeedLevel_UltraHD,
//    EnumSpeedLevel_8K
//};
//
//@interface SpeedTestLevel : NSObject
//+(UIImage *)getLevelIconWithSpeed:(NSInteger)download;
//+(UIImage *)getLevelLineWithSpeed:(NSInteger)download;
//@end
