//
//  SpeedTestHistory.m
//  Multy
//
//  Created by 吳昌鴻 on 2018/1/9.
//  Copyright © 2018年 Nick. All rights reserved.
//

#import "SpeedTestHistory.h"
@implementation SpeedTestRecord

-(instancetype)init{
    self = [super init];
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [dateFormatter stringFromDate: currentTime];
    self.date = dateString;

    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"HH:mm"];
//    if ([self is24HourTimeFormatter]) {
//        [timeFormatter setDateFormat:@"HH:mm:ss"];
//    }else{
//        [timeFormatter setDateFormat:@"hh:mm:ss a"];
//    }
    NSString *timeString = [timeFormatter stringFromDate: currentTime];
    [[NSUserDefaults standardUserDefaults] synchronize];
    self.time = timeString;
    self.uploadSpeed = @"N/A";
    self.downloadSpeed = @"N/A";
    return self;
}
- (bool)is24HourTimeFormatter{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateStyle:NSDateFormatterNoStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    NSString *dateString = [formatter stringFromDate:[NSDate date]];
    NSRange amRange = [dateString rangeOfString:[formatter AMSymbol]];
    NSRange pmRange = [dateString rangeOfString:[formatter PMSymbol]];
    BOOL is24h = (amRange.location == NSNotFound && pmRange.location == NSNotFound);
    return is24h;
}
@end

@implementation SpeedTestHistory

+(void)delAllRecords{
    NSArray* arry = [[NSArray alloc] init];
    [[NSUserDefaults standardUserDefaults] setObject:arry forKey:@"SpeedTestRecord"];
}

+(NSMutableArray *)getAllRecords{
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"SpeedTestRecord"] == nil){

        NSArray* arry = [[NSUserDefaults standardUserDefaults] objectForKey:@"SpeedTestRecord"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return [self dic2record:arry];

    }else{
        NSMutableArray* arry= [[NSUserDefaults standardUserDefaults] objectForKey:@"SpeedTestRecord"];
        return [self dic2record:arry];
    }
}
+(void)setAllRecords:(NSArray *)records {
    NSMutableArray *array = [NSMutableArray array];
    for (SpeedTestRecord *record in records) {
        NSDictionary *temp = @{@"date":record.date, @"time":record.time,@"download":record.downloadSpeed,@"upload":record.uploadSpeed};
        [array addObject:temp];
    }
    [[NSUserDefaults standardUserDefaults] setObject:array forKey:@"SpeedTestRecord"];
}
+(NSMutableArray *)dic2record:(NSArray *)dicArry{
    NSMutableArray *array = [NSMutableArray array];
    for (NSDictionary *temp in dicArry) {
        SpeedTestRecord *record = [[SpeedTestRecord alloc]init];
        record.time = temp[@"time"];
        record.date = temp[@"date"];
        record.downloadSpeed = temp[@"download"];
        record.uploadSpeed = temp[@"upload"];
        [array addObject:record];
    }
    return array;
}
+(void)addRecord:(SpeedTestRecord *)record{
//    if (record.date == nil || record.time == nil) {
//        NSDate *currentTime = [NSDate date];
//        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//        [dateFormatter setDateFormat:@"yyyy/MM/dd"];
//        NSString *dateString = [dateFormatter stringFromDate: currentTime];
//        record.date = dateString;
//
//        NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
//        if ([self is24HourTimeFormatter]) {
//            [timeFormatter setDateFormat:@"HH:mm"];
//        }else{
//            [timeFormatter setDateFormat:@"hh:mm"];
//        }
//        NSString *timeString = [timeFormatter stringFromDate: currentTime];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//        record.time = timeString;
//    }
    NSMutableArray* arry = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"SpeedTestRecord"]];
    NSDictionary *temp = @{@"time":record.time, @"date":record.date, @"download":record.downloadSpeed, @"upload":record.uploadSpeed};
    [arry addObject:temp];
    [[NSUserDefaults standardUserDefaults] setObject:arry forKey:@"SpeedTestRecord"];
}


@end
