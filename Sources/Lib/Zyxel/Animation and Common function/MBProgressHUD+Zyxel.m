//
//  NSObject+MBProgressHUD_Zyxel.m
//  ZYXEL
//
//  Created by Kevin on 19/05/2017.
//  Copyright © 2017 Nick. All rights reserved.
//

#import "MBProgressHUD+Zyxel.h"

@implementation NSObject (Zyxel)
+ (MBProgressHUD *)showGlobalProgressHUDWithTitle:(NSString *)title {
//    UIWindow *window = [[[UIApplication sharedApplication] windows] lastObject];
//    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:window animated:YES];
//    hud.labelText = title;
    UIView *window = [UIApplication sharedApplication].keyWindow;
    MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:window animated:YES];
    //[hud show:YES];
    [hud showAnimated:YES];//amy modify
    return hud;
}

+ (void)dismissGlobalHUD {
//    UIWindow *window = [[[UIApplication sharedApplication] windows] lastObject];
    UIView *window = [UIApplication sharedApplication].keyWindow;
    [MBProgressHUD hideHUDForView:window animated:YES];
}
+(void)startMBProgressHUD{
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD showGlobalProgressHUDWithTitle:@""];
    });
}
+(void)stopMBProgressHUD {
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD dismissGlobalHUD];
    });
}
+(void)startMBProgressHUD:(NSString *)title{
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD showGlobalProgressHUDWithTitle:title];
    });
}
@end
