//
//  NSObject+MBProgressHUD_Zyxel.h
//  ZYXEL
//
//  Created by Kevin on 19/05/2017.
//  Copyright © 2017 Nick. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MBProgressHUD/MBProgressHUD.h>

@interface MBProgressHUD (Zyxel)
//+ (MBProgressHUD *)showGlobalProgressHUDWithTitle:(NSString *)title;
//+ (void)dismissGlobalHUD;
+ (void)startMBProgressHUD;
+ (void)stopMBProgressHUD;
+(void)startMBProgressHUD:(NSString *)title;
@end
