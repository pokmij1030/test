//
//  SpeedTest.m
//  test
//
//  Created by Kevin on 2015/2/12.
//  Copyright (c) 2015年 ZyXEL. All rights reserved.
//

#import "SpeedTest.h"
//#import "MBProgressHUD.h"
//#import "MBProgressHUD+Zyxel.h"
//#include "CDevice.h"
@interface SpeedTest () <NSURLConnectionDataDelegate, NSXMLParserDelegate, NSURLSessionDelegate, NSURLSessionDataDelegate>
//@property NSMutableArray *serverList;
@property NSArray *top20ServerList;
@property OoklaClient *client;

@property (nonatomic) NSUInteger length;
@property  NSDate *startTime;
@property  NSDate *endTime;
@property NSDate *lastUpdateDate;
@property NSLock* lock;
@property NSMutableArray* dlConnections;
@property NSMutableArray* ulConnections;
@property NSMutableArray* dlrequests;
@property NSMutableArray* ulrequests;

@property NSTimeInterval interval_t;
@property NSData *requestData;

@property NSXMLParser *clientPaser;
@property NSXMLParser *serverPaser;
@property BOOL isDownloading;
@property BOOL isUploading;

@end

#define PI 3.14159265358979323846
#define SERVER_DATA_SIZE_BYTE 300

long long shouldDownLoadLength[10];
long long didDownLoadLength[10];
static bool dlFinish[10];
static bool ulFinish[10];
float intervalToStop;
float intervalTotal;
float caculateCount;
float caculateTotal;
dispatch_semaphore_t sem;
//dispatch_queue_t queue =  dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//dispatch_group_t group = dispatch_group_create();
int count_t;

@implementation NSURLRequest(DataController)
+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString *)host
{
    return YES;
}
@end

@implementation SpeedTest
@synthesize timeOut;
@synthesize numofCycle;
@synthesize numOfThread;
@synthesize testImagePixel;
@synthesize testWay;

-(id)init{
    self.numOfThread = 4;
    self.numofCycle = 1000;
    self.testImagePixel = 1000;
    self.timeOut = 15;
    self.requestData = [[NSMutableData alloc]init];
    self.serverList = [[NSMutableArray alloc]init];
    self.client =  [[OoklaClient alloc]init];
    self.lastUpdateDate = [NSDate date];
    self.autoSelectServerEnable = true;
    sem = dispatch_semaphore_create(1);
    self.caculateInterval = 1.0;
    caculateCount = 0;
    caculateTotal = 0;
    _isDownloading = false;
    _isUploading = false;
    testWay = SPEEDTESTWAY_ClinetToInternet;

    return self;
}
-(void)downLoadClientInfo:(NSString *)path{
    self.clientInfoPath = path;
}
-(void)downLoadServerInfo:(NSString *)path{
    self.serverInfoPath = path;
}
-(void)paserServerInfo:(NSString *)clientInfoPath withClientInfo:(NSString *)serverInfoPath{
    [self.serverList removeAllObjects];
    NSData *temp;
    if (clientInfoPath==nil && self.clientInfoPath==nil){
        NSError *err = [NSError errorWithDomain:SpeedTestErrorDomain code:SpeedTestDownloadPaserClientInfoError userInfo:nil];
        [self.delegate speedtest:self didFailWithError:err];
        return;
    }
    if (clientInfoPath==nil) {
        temp = [NSData dataWithContentsOfFile:self.clientInfoPath];
    }else{
        temp = [NSData dataWithContentsOfFile:self.clientInfoPath];
    }
    self.clientPaser = [[NSXMLParser alloc] initWithData:temp];
    self.clientPaser.delegate = self;
    [self.clientPaser parse];
    
    
    if (serverInfoPath==nil && self.serverInfoPath==nil) {
        NSError *err = [NSError errorWithDomain:SpeedTestErrorDomain code:SpeedTestDownloadPaserServerInfoError userInfo:nil];
        [self.delegate speedtest:self didFailWithError:err];
        return;
    }
    if (serverInfoPath==nil) {
        temp = [NSData dataWithContentsOfFile:self.serverInfoPath];
    }else{
        temp = [NSData dataWithContentsOfFile:self.serverInfoPath];
    }
    self.serverPaser = [[NSXMLParser alloc] initWithData:temp];
    self.serverPaser.delegate = self;
    [self.serverPaser parse];
}
- (void)getClientInfo:(void(^)(NSError *error))completionHandler {
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask *task =  [session dataTaskWithURL:[NSURL URLWithString:@"https://www.speedtest.net/speedtest-config.php"] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSData *clientData = data;
        if(data.length){
            [clientData writeToFile:self.clientInfoPath atomically:YES];
            NSLog(@"Downloaded clientInfo file successfully.");
            completionHandler(nil);
        }else{
            NSLog(@"Download clientInfo fail.");
            NSError *err = [NSError errorWithDomain:SpeedTestErrorDomain code:SpeedTestDownloadClientInfoError userInfo:nil];
            [self.delegate speedtest:self didFailWithError:err];
            completionHandler(err);
            return;
        }
    }];
    [task resume];
}

- (void)getServerInfo:(void(^)(NSError *error))completionHandler {
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask *task =  [session dataTaskWithURL:[NSURL URLWithString:@"https://www.speedtest.net/speedtest-servers-static.php"] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//        NSLog(@"%@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
        NSData *serverData = data;
        
        if(serverData.length < SERVER_DATA_SIZE_BYTE || error){
            
            NSURLSessionDataTask *task =  [session dataTaskWithURL:[NSURL URLWithString:@"https://www.speedtest.net/speedtest-servers.php"] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                NSData *serverData = data;
                if(serverData.length < SERVER_DATA_SIZE_BYTE || error){
                    NSLog(@"ookla server is down");
                    NSString *serverFilePath =  [[NSBundle mainBundle] pathForResource:@"ookla_servers" ofType:@"xml"];
                    [[NSFileManager defaultManager] copyItemAtPath:serverFilePath toPath:serverFilePath error:&error];
                    self.serverInfoPath = serverFilePath;
                    if (serverFilePath == nil) {
                        NSLog(@"Download serverInfo fail. Can't get cache file");
                        completionHandler(error);
                    }else{
                        NSLog(@"Download serverInfo fail. Use cache file");
                        completionHandler(nil);
                    }
                }
                else{
                    [serverData writeToFile:self.serverInfoPath atomically:YES];
                    NSLog(@"Download serverInfo file successfully.");
                    completionHandler(nil);
                }
            }];
            [task resume];
            
        }else {
            [serverData writeToFile:self.serverInfoPath atomically:YES];
            NSLog(@"Download serverInfo file successfully.");
            completionHandler(nil);
        }
    }];
    [task resume];
}

-(void)prepareForSpeedTest{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingString:@"/"];
    NSLog(@"----%@",filePath);

    NSString *clientFilePath = [filePath stringByAppendingPathComponent:@"client.xml"];
    NSString *serverFilePath = [filePath stringByAppendingPathComponent:@"servers.xml"];
    if (self.clientInfoPath==nil) {
        self.clientInfoPath = clientFilePath;
    }
    if (self.serverInfoPath==nil) {
        self.serverInfoPath = serverFilePath;
    }
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSDate *modificationDate;
    if ([fileManager fileExistsAtPath:self.serverInfoPath]) {
//        [fileManager copyItemAtPath:serverFilePath toPath:serverFilePathDocument error:&error];
        NSDictionary *attri = [fileManager attributesOfItemAtPath:self.serverInfoPath error:nil];
        NSLog(@"%@", [fileManager attributesOfItemAtPath:self.serverInfoPath error:nil]);
        NSLog(@"%@", [attri objectForKey:NSFileModificationDate]);
        modificationDate = [attri objectForKey:NSFileModificationDate];
    }
    NSLog(@"interval:%f",[[NSDate date]timeIntervalSinceDate:modificationDate]);
    
    
    if ([[NSDate date]timeIntervalSinceDate:modificationDate] > 60 * 60 * 24 || ![fileManager fileExistsAtPath:self.serverInfoPath]) {
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        [self getServerInfo:^(NSError *error) {
            if (error) {
                NSError *err = [NSError errorWithDomain:SpeedTestErrorDomain code:SpeedTestDownloadServerInfoError userInfo:nil];
                [self.delegate speedtest:self didFailWithError:err];
            }
            dispatch_semaphore_signal(sema);
        }];
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    }
    dispatch_semaphore_t sema1 = dispatch_semaphore_create(0);
    [self getClientInfo:^(NSError *error) {
        [self paserServerInfo:nil withClientInfo:nil];
        dispatch_semaphore_signal(sema1);
    }];
    dispatch_semaphore_wait(sema1, DISPATCH_TIME_FOREVER);
}
-(void)prepareForSpeedTest:(void(^)(void))completion {
    [self.delegate speedtest:self didFailWithError:nil];    
    [self prepareForSpeedTest];
    completion();
}
-(void)operationQueueDownload{
    for (int i =0 ; i < self.numOfThread; i++) {
        
    }
}
-(void)startDownloadTest_ClientToInternet{
    for (int i =0 ; i < self.numOfThread; i++) {
        [self.dlConnections addObject:[NSURLConnection alloc]];
        
        OoklaServer *server;
        if (self.manualSelectServer==nil) {
            server = [self.top20ServerList objectAtIndex:i];
        }else{
            server = self.manualSelectServer;
        }
        if (server == nil) {
            //[MBProgressHUD startMBProgressHUD]; //amy mark
            [self.delegate speedtest:self loading:true];
            [self prepareForSpeedTest];
            //[MBProgressHUD stopMBProgressHUD]; //amy mark
            [self.delegate speedtest:self loading:false];
        }
        NSString *serverURL = [NSString stringWithString:server.url];
        NSString *fileName = [NSString stringWithFormat:@"random%dx%d.jpg",self.testImagePixel,self.testImagePixel];
        NSString *str = [serverURL lastPathComponent];
        NSString *filePath;
        if ([str rangeOfString:@"upload"].location != NSNotFound) {
            filePath = [serverURL stringByReplacingOccurrencesOfString:str withString:fileName];
        }else{
            filePath = [serverURL stringByReplacingOccurrencesOfString:@"upload.php" withString:fileName];
        }
        
        NSURL *url = [NSURL URLWithString:filePath];
        //NSURL *url = [NSURL URLWithString:@"http://NBG6816.zyxel.com/download"];
        [self.dlrequests addObject:[NSURLRequest requestWithURL:url]];
    }
    //---
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    myQueue.maxConcurrentOperationCount = numOfThread;
    
    _isDownloading = true;
    for (int i =0 ; i < self.numOfThread; i++) {
        
        NSBlockOperation *operation = [[NSBlockOperation alloc] init];
        __weak NSBlockOperation *weakOperation = operation;
        [operation addExecutionBlock: ^ {
            for(int j = 0 ; j < self.numofCycle ; j++){
                NSURLConnection *connection = [self.dlConnections objectAtIndex:i];
                NSURLRequest *request = [self.dlrequests objectAtIndex:i];
                connection = [connection initWithRequest:request delegate:self];
                while(!dlFinish[i]){
                    if ([weakOperation isCancelled]) {
                        [connection cancel];
                        //                        [self.delegate currentDownloadSpeed:0.0 completed:true];
                        //                        NSLog(@"download test done");
                        return;
                    }
                    [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
                }
                dlFinish[i] = false;
            }
        }];
        [myQueue addOperation:operation];
        
    }
    
    //    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    //        [myQueue cancelAllOperations];
    //        NSLog(@"download test done");
    //
    //    });
    //[myQueue waitUntilAllOperationsAreFinished];
    [NSThread sleepForTimeInterval:self.timeOut];
    [myQueue cancelAllOperations];
    [NSThread sleepForTimeInterval:1.5];
    float avgSpeed = caculateTotal / caculateCount;
    _isDownloading = false;
    [self.delegate speedtest:self downloadSpeed:avgSpeed completion:true];
    //[self.delegate currentDownloadSpeed:avgSpeed completion:true];
    NSLog(@"download test done");
    
    return;
/*
    //---
    dispatch_queue_t queue =  dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_group_t group = dispatch_group_create();
    
    shouldWriteToBuffer = true;
    for (int i =0 ; i < self.numOfThread; i++) {
        dispatch_group_async(group, queue, ^{
            for(int j = 0 ; j < self.numofCycle ; j++){
                NSURLConnection *connection = [self.dlConnections objectAtIndex:i];
                NSURLRequest *request = [self.dlrequests objectAtIndex:i];
                connection = [connection initWithRequest:request delegate:self];
                while(!dlFinish[i]){
                    [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
                }
                dlFinish[i] = false;
                shouldWriteToBuffer = false;
                //                dispatch_group_notify(group, queue, ^{
                //                    NSLog(@"group notify");
                //                });
            }
        });
    }
    
    dispatch_group_wait(group, DISPATCH_TIME_FOREVER);
    NSLog(@"download test done");
    [self startUploadTest];
    //    dispatch_barrier_async(queue, ^{
    //        NSLog(@"up load");
    //        [self startUploadTest];
    //    });
    
    NSLog(@"Thread:----%@",[NSThread currentThread]);
*/
}

//-(void)startDownloadTest_ClientToGateway{
//    CDevice *device = new CDevice;
//    device->SetGwSpeedTestEnable(false);
//    bool ret = device->SetGwSpeedTestEnable(true);
//    if (ret>0) {
//        int gatewayPort = device->GetGwSpeedTestPort();
//
//    for (int i =0 ; i < self.numOfThread; i++) {
//        [self.dlConnections addObject:[NSURLConnection alloc]];
//
//        NSString *serverURL = [NSString stringWithFormat:@"%s", device->getGatewayLANIP().c_str()];
//        serverURL = [NSString stringWithFormat:@"%@:%d", serverURL, gatewayPort];
//        NSString *filePath = [NSString stringWithFormat:@"http://%@/speedtest/download.php", serverURL];
//        NSURL *url = [NSURL URLWithString:filePath];
//        //NSURL *url = [NSURL URLWithString:@"http://192.168.1.1/speedtest/download.php"];
//        [self.dlrequests addObject:[NSURLRequest requestWithURL:url]];
//    }
//        //---
//        NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
//        myQueue.maxConcurrentOperationCount = numOfThread;
//
//        _isDownloading = true;
//        for (int i =0 ; i < self.numOfThread; i++) {
//
//            NSBlockOperation *operation = [[NSBlockOperation alloc] init];
//            __weak NSBlockOperation *weakOperation = operation;
//            [operation addExecutionBlock: ^ {
//                for(int j = 0 ; j < self.numofCycle ; j++){
//                    NSURLConnection *connection = [self.dlConnections objectAtIndex:i];
//                    NSURLRequest *request = [self.dlrequests objectAtIndex:i];
//                    connection = [connection initWithRequest:request delegate:self];
//                    while(!dlFinish[i]){
//                        if ([weakOperation isCancelled]) {
//                            [connection cancel];
//                            return;
//                        }
//                        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
//                    }
//                    dlFinish[i] = false;
//                }
//            }];
//            [myQueue addOperation:operation];
//        }
//
//        [NSThread sleepForTimeInterval:self.timeOut];
//        [myQueue cancelAllOperations];
//        [NSThread sleepForTimeInterval:1.5];
//        float avgSpeed = caculateTotal / caculateCount;
//        _isDownloading = false;
//        [self.delegate speedtest:self downloadSpeed:avgSpeed completion:true];
//        //[self.delegate currentDownloadSpeed:avgSpeed completion:true];
//        NSLog(@"download test done");
//
//        return;
//
//    }   //end if (ret>0) {
//}

-(NSData*)create20mbRandomNSData
{
    int twentyMb           = 20971520;
    NSMutableData* theData = [NSMutableData dataWithCapacity:twentyMb];
    for( unsigned int i = 0 ; i < twentyMb/4 ; ++i )
    {
        u_int32_t randomBits = arc4random();
        [theData appendBytes:(void*)&randomBits length:4];
    }
    return theData;
}

//-(void)startDownloadTest_GatewayToInternet{
//    NSString *serverListURL;
//    for(int i = 0; i < numOfThread; i++){
//        OoklaServer *server;
//        if (self.manualSelectServer==nil) {
//            server = [self.top20ServerList objectAtIndex:i];
//        }else{
//            server = self.manualSelectServer;
//        }
//        NSString *serverURL = [server.url stringByReplacingOccurrencesOfString:@"http://" withString:@""];
//        serverURL = [serverURL stringByReplacingOccurrencesOfString:@"/speedtest/upload.php" withString:@""];
//        if (i == 0) {
//            serverListURL = [NSString stringWithString:serverURL];
//        }else{
//            serverListURL = [serverListURL stringByAppendingFormat:@",%@",serverURL];
//        }
//    }
//
//    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
//    NSBlockOperation *operation = [[NSBlockOperation alloc] init];
//    //__weak NSBlockOperation *weakOperation = operation;
//
//    [operation addExecutionBlock: ^ {
//        NSDate *begin = [NSDate date];
//        __block float period;
//        CDevice myDevice = *new CDevice;
//        myDevice.SetSpeedTest([serverListURL UTF8String], 0);
//        myDevice.GetSpeedTestDownload();
//        sleep(self.caculateInterval);
//        float downloadSpeed;
//        string tmp;
//        int flag = 0;
//        float total = 0;
//        int count = 0;
//        //int zerocount = 0;
//        while (!flag) {
//            tmp = myDevice.GetSpeedTestDownload();
//            downloadSpeed = atof(tmp.c_str());
//            if (downloadSpeed >= 0) {
//                if (downloadSpeed > 0) {
//                    count ++;
//                    if (count >=2) {
//                        total += downloadSpeed;
//                    }
//                    [self.delegate speedtest:self downloadSpeed:downloadSpeed completion:false];
//                }
//                period = [[NSDate date]timeIntervalSinceDate:begin];
//                NSLog(@"period = %f, count = %d", period, (int)count);
//                flag = myDevice.GetSpeedTestEnd();
//                if ((period <= self.timeOut) && flag==1) {
//                    myDevice.SetSpeedTest([serverListURL UTF8String], 0);
//                    flag = 0;
//                }
//                else if (period > self.timeOut){
//                    flag = 1;
//                }
//                else{
//                    flag = 0;
//                }
//            }
//            sleep(1);
//        }
//        tmp = myDevice.GetSpeedTestDownload();
//        downloadSpeed = atof(tmp.c_str());
//        [self.delegate speedtest:self downloadSpeed:total/(count-1) completion:true];
//    }];
//    [myQueue addOperation:operation];
//
////    [NSThread sleepForTimeInterval:self.timeOut];
////    [myQueue cancelAllOperations];
////    [NSThread sleepForTimeInterval:1.5];
//     NSLog(@"download test done");
//}
-(void)startDownloadTest{
    caculateTotal = 0;
    caculateCount = 0;
    self.maxDownloadSpeed = 0.0;
    self.dlConnections = [[NSMutableArray alloc]init];
    self.dlrequests = [[NSMutableArray alloc]init];

    [self startDownloadTest_ClientToInternet];

//    switch (testWay) {
//        case SPEEDTESTWAY_ClinetToInternet:
//            [self startDownloadTest_ClientToInternet];
//            break;
//        case SPEEDTESTWAY_ClientToGateway:
//            [self startDownloadTest_ClientToGateway];
//            break;
//        case SPEEDTESTWAY_GatewayToInternet:
//            [self startDownloadTest_GatewayToInternet];
//            break;
//        default:
//            break;
//    }    
}

-(void)startUploadTest_ClientToInternet{

    _isUploading = true;
    _requestData = [self create20mbRandomNSData];
    
    for (int i =0 ; i < self.numOfThread; i++) {
        [self.ulConnections addObject:[NSURLConnection alloc]];
        
        OoklaServer *server;
        if (self.manualSelectServer==nil) {
            server = self.numOfThread < self.top20ServerList.count ? [self.top20ServerList objectAtIndex:i] : [self.top20ServerList firstObject];
        }else{
            server = self.manualSelectServer;
        }
        
        NSString *serverURL = [NSString stringWithString:server.url];
        //NSString *serverURL = @"http://NBG6816.zyxel.com/upload.php";
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:serverURL]];
        [request setHTTPMethod:@"POST"];
        
        NSMutableData *body = [NSMutableData data];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
        
        //The file to upload
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: attachment; name=\"image\"; filename=\"image.jpg\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:_requestData]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        // close the form
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // set request body
        [request setHTTPBody:body];
        
        [self.ulrequests addObject:request];
        
    }
    //---
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    myQueue.maxConcurrentOperationCount = numOfThread;
    
    for (int i =0 ; i < self.numOfThread; i++) {
        
        NSBlockOperation *operation = [[NSBlockOperation alloc] init];
        __weak NSBlockOperation *weakOperation = operation;
        [operation addExecutionBlock: ^ {
            for(int j = 0 ; j < self.numofCycle ; j++){
                NSURLConnection *connection = [self.ulConnections objectAtIndex:i];
                NSURLRequest *request = [self.ulrequests objectAtIndex:i];
                connection = [connection initWithRequest:request delegate:self];
                while(!ulFinish[i]){
                    if ([weakOperation isCancelled]) {
                        [connection cancel];
                        //                        [self.delegate currentUploadSpeed:0.0 completed:true];
                        //                        NSLog(@"upload test done");
                        return;
                    }
                    [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
                }
                ulFinish[i] = false;
            }
        }];
        [myQueue addOperation:operation];
        
    }
    
    //    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    //        [myQueue cancelAllOperations];
    //        NSLog(@"upload test done");
    //
    //    });
    
    //    [myQueue waitUntilAllOperationsAreFinished];
    [NSThread sleepForTimeInterval:self.timeOut];
    [myQueue cancelAllOperations];
    [NSThread sleepForTimeInterval:1.5];
    float avgSpeed = caculateTotal / caculateCount;
    _isUploading = false;
    [self.delegate speedtest:self uploadSpeed:avgSpeed completion:true];
    //[self.delegate currentUploadSpeed:avgSpeed completion:true];
    NSLog(@"upload test done");
    return;
    

    dispatch_queue_t queue =  dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_group_t group = dispatch_group_create();
    for (int i =0 ; i < self.numOfThread; i++) {
        dispatch_group_async(group, queue, ^{
            for(int j = 0 ; j < self.numofCycle ; j++){
                NSURLConnection *connection = [self.ulConnections objectAtIndex:i];
                NSURLRequest *request = [self.ulrequests objectAtIndex:i];
                connection = [connection initWithRequest:request delegate:self];
                while(!ulFinish[i]){
                    [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
                }
                ulFinish[i] = false;
                //                dispatch_group_notify(group, queue, ^{
                //                    NSLog(@"group notify");
                //                });
            }
            
        });
    }
    
    dispatch_group_wait(group, DISPATCH_TIME_FOREVER);
    NSLog(@"upload test done");
    
    
    // Configure your request here.  Set timeout values, HTTP Verb, etc.
    //    NSURLConnection *connection = [NSURLConnection connectionWithRequest:request delegate:self];
    //    while (1) {
    //        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    //    }

}

//-(void)startUploadTest_ClientToGateway{
//
//    CDevice *device = new CDevice;
//    int gatewayPort = device->GetGwSpeedTestPort();
//    if (gatewayPort>0) {
//
//    _isUploading = true;
//    _requestData = [self create20mbRandomNSData];
//
//    for (int i =0 ; i < self.numOfThread; i++) {
//        [self.ulConnections addObject:[NSURLConnection alloc]];
//
//        NSString *serverURL = [NSString stringWithFormat:@"%s", device->getGatewayLANIP().c_str()];
//        serverURL = [NSString stringWithFormat:@"%@:%d", serverURL, gatewayPort];
//        NSString *filePath = [NSString stringWithFormat:@"http://%@/speedtest/upload.php", serverURL];
//        //NSString *serverURL = @"http://192.168.1.1/speedtest/upload.php";
//        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
//        [request setURL:[NSURL URLWithString:filePath]];
//        [request setHTTPMethod:@"POST"];
//
//        NSMutableData *body = [NSMutableData data];
//
//        NSString *boundary = @"---------------------------14737809831466499882746641449";
//        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
//        [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
//
//        //The file to upload
//        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: attachment; name=\"image\"; filename=\"image.jpg\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
//        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//        [body appendData:[NSData dataWithData:_requestData]];
//        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//
//        // close the form
//        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//
//        // set request body
//        [request setHTTPBody:body];
//
//        [self.ulrequests addObject:request];
//
//    }
//    //---
//    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
//    myQueue.maxConcurrentOperationCount = numOfThread;
//
//    for (int i =0 ; i < self.numOfThread; i++) {
//
//        NSBlockOperation *operation = [[NSBlockOperation alloc] init];
//        __weak NSBlockOperation *weakOperation = operation;
//        [operation addExecutionBlock: ^ {
//            for(int j = 0 ; j < self.numofCycle ; j++){
//                NSURLConnection *connection = [self.ulConnections objectAtIndex:i];
//                NSURLRequest *request = [self.ulrequests objectAtIndex:i];
//                connection = [connection initWithRequest:request delegate:self];
//                while(!ulFinish[i]){
//                    if ([weakOperation isCancelled]) {
//                        [connection cancel];
//                        //                        [self.delegate currentUploadSpeed:0.0 completed:true];
//                        //                        NSLog(@"upload test done");
//                        return;
//                    }
//                    [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
//                }
//                ulFinish[i] = false;
//            }
//        }];
//        [myQueue addOperation:operation];
//
//    }
//
//    [NSThread sleepForTimeInterval:self.timeOut];
//    [myQueue cancelAllOperations];
//    [NSThread sleepForTimeInterval:1.5];
//    float avgSpeed = caculateTotal / caculateCount;
//    _isUploading = false;
//    [self.delegate speedtest:self uploadSpeed:avgSpeed completion:true];
//    //[self.delegate currentUploadSpeed:avgSpeed completion:true];
//    NSLog(@"upload test done");
//    return;
//
//
//    dispatch_queue_t queue =  dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//    dispatch_group_t group = dispatch_group_create();
//    for (int i =0 ; i < self.numOfThread; i++) {
//        dispatch_group_async(group, queue, ^{
//            for(int j = 0 ; j < self.numofCycle ; j++){
//                NSURLConnection *connection = [self.ulConnections objectAtIndex:i];
//                NSURLRequest *request = [self.ulrequests objectAtIndex:i];
//                connection = [connection initWithRequest:request delegate:self];
//                while(!ulFinish[i]){
//                    [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
//                }
//                ulFinish[i] = false;
//            }
//
//        });
//    }
//
//    dispatch_group_wait(group, DISPATCH_TIME_FOREVER);
//    NSLog(@"upload test done");
//
//    }   //end if (gatewayPort>0)
//}

//-(void)startUploadTest_GatewayToInternet{
//    NSString *serverListURL;
//    for(int i = 0; i < numOfThread; i++){
//        OoklaServer *server;
//        if (self.manualSelectServer==nil) {
//            server = [self.top20ServerList objectAtIndex:i];
//        }else{
//            server = self.manualSelectServer;
//        }
//        NSString *serverURL = [server.url stringByReplacingOccurrencesOfString:@"http://" withString:@""];
//        serverURL = [serverURL stringByReplacingOccurrencesOfString:@"/speedtest/upload.php" withString:@""];
//        if (i == 0) {
//            serverListURL = [NSString stringWithString:serverURL];
//        }else{
//            serverListURL = [serverListURL stringByAppendingFormat:@",%@",serverURL];
//        }
//    }
//
//    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
//    NSBlockOperation *operation = [[NSBlockOperation alloc] init];
//    //__weak NSBlockOperation *weakOperation = operation;
//    
//    [operation addExecutionBlock: ^ {
//        NSDate *begin = [NSDate date];
//        __block float period;
//        CDevice myDevice = *new CDevice;
//        myDevice.SetSpeedTest([serverListURL UTF8String], 1);
//        myDevice.GetSpeedTestUpload();
//        sleep(self.caculateInterval);
//        float uploadSpeed;
//        string tmp;
//        int flag = 0;
//        float total = 0;
//        int count = 0;
//        //int zerocount = 0;
//        while (!flag) {
//            tmp = myDevice.GetSpeedTestUpload();
//            uploadSpeed = atof(tmp.c_str());
//            if (uploadSpeed >= 0) {
//                if (uploadSpeed > 0) {
//                    count ++;
//                    if (count >=2) {
//                        total += uploadSpeed;
//                    }
//                    [self.delegate speedtest:self uploadSpeed:uploadSpeed completion:false];
//                }
//                period = [[NSDate date]timeIntervalSinceDate:begin];
//                NSLog(@"period = %f, count = %d", period, (int)count);
//                flag = myDevice.GetSpeedTestEnd();
//                if ((period <= self.timeOut) && flag==1) {
//                    myDevice.SetSpeedTest([serverListURL UTF8String], 1);
//                    flag = 0;
//                    NSLog(@"Speed 1");
//                }
//                else if(period > self.timeOut){
//                    flag = 1;
//                    NSLog(@"Speed 2");
//                }
//                else{
//                    flag = 0;
//                    NSLog(@"Speed 3");
//                }
//            }
//            sleep(1);
//        }
//        tmp = myDevice.GetSpeedTestUpload();
//        uploadSpeed = atof(tmp.c_str());
//        [self.delegate speedtest:self uploadSpeed:total/(count-1) completion:true];
//    }];
//    [myQueue addOperation:operation];
//    
//    //    [NSThread sleepForTimeInterval:self.timeOut];
//    //    [myQueue cancelAllOperations];
//    //    [NSThread sleepForTimeInterval:1.5];
//    NSLog(@"upload test done");
//}
- (void) startUploadTest{
    caculateTotal = 0;
    caculateCount = 0;
    self.maxUploadSpeed = 0.0;
    self.ulConnections = [[NSMutableArray alloc]init];
    self.ulrequests = [[NSMutableArray alloc]init];
    self.length = 0;
    self.interval_t = 0;
    
    switch (testWay) {
        case SPEEDTESTWAY_ClinetToInternet:
            [self startUploadTest_ClientToInternet];
            break;
        case SPEEDTESTWAY_ClientToGateway:
//            [self startUploadTest_ClientToGateway];
            break;
        case SPEEDTESTWAY_GatewayToInternet:
//            [self startUploadTest_GatewayToInternet];
            break;
        default:
            break;
    }

}

-(NSArray*)getTop20ServerList{

    for(OoklaServer *server in self.serverList){
        double Radius = 6371;
        double RadLat = (atof([server.lat UTF8String]))*(PI/180);
        double RadLong = (atof([server.lon UTF8String]))*(PI/180);
        double RadLat_Client = (atof([_client.lat UTF8String]))*(PI/180);
        double RadLong_Client = (atof([_client.lon UTF8String]))*(PI/180);
        
        double deltaLat = RadLat_Client - RadLat;
        double deltaLong = RadLong_Client - RadLong;
        
        double a = pow((sin(deltaLat/2)),2) + cos(RadLat)*cos(RadLat_Client)* pow((sin(deltaLong/2)),2);
        double c = 2*atan2(sqrt(a), sqrt(1-a));
        double d = Radius*c;
        [server setDistance:d];
    }
    NSSortDescriptor *sortDescriptor=[[NSSortDescriptor alloc]initWithKey:@"distance" ascending:YES];
    NSArray *sortDescriptors=[NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArr=[self.serverList sortedArrayUsingDescriptors:sortDescriptors];
    
    NSMutableArray *top20 = [[NSMutableArray alloc]init];

    if ([sortedArr count]==0) {
        return nil;
    }
    unsigned long MAX = [sortedArr count] > 20 ? 20 : [sortedArr count];
    for (int i = 0 ; i < MAX ; i++) {
            OoklaServer *checkurl = [sortedArr objectAtIndex:i];
            NSURLRequest * urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:checkurl.url]cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:1.5];
            NSURLResponse * response = nil;
            NSError * error = nil;
            NSData * data = [NSURLConnection sendSynchronousRequest:urlRequest
                                                  returningResponse:&response
                                                              error:&error];
            if (error == nil) {
                NSLog(@">>>>>>>>>>>urlRequest URL[%d] is OK!", i);
                [top20 addObject:[sortedArr objectAtIndex:i]];
            }
            else{
                NSLog(@">>>>>>>>>>>urlRequest URL is not OK!\n%@", checkurl.url);
        }
    }
    self.top20ServerList = top20;
    return top20;
}
#pragma mark - NSXMLPaserDelegate methods
-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if(parser==self.serverPaser){
        if ([elementName isEqualToString:@"server"]) {
            OoklaServer *server = [[OoklaServer alloc]init];
            [server setSponsor:[attributeDict objectForKey:@"sponsor"]];
            [server setUrl:[attributeDict objectForKey:@"url"]];
            //[server setUrl2:[attributeDict objectForKey:@"url2"]];
            [server setLat:[attributeDict objectForKey:@"lat"]];
            [server setLon:[attributeDict objectForKey:@"lon"]];
            [server setName:[attributeDict objectForKey:@"name"]];
            [server setCountry:[attributeDict objectForKey:@"country"]];
            [server setCountry_code:[attributeDict objectForKey:@"cc"]];
            [server setIndex:(NSInteger)[attributeDict objectForKey:@"id"]];
            [self.serverList addObject:server];
        }
    }else{
        if ([elementName isEqualToString:@"client"]) {
            [self.client setIp:[attributeDict objectForKey:@"ip"]];
            [self.client setLat:[attributeDict objectForKey:@"lat"]];
            [self.client setLon:[attributeDict objectForKey:@"lon"]];
            [self.client setIsp:[attributeDict objectForKey:@"isp"]];
        }
    }
//    NSLog(@"didStartElement");
//    NSLog(@"elementName %@", elementName);
//    for(id key in attributeDict)
//    {
//        NSLog(@"attribute: %@ , key: %@", [attributeDict objectForKey:key],key);
//    }
}
#pragma mark - NSURLConnectionDataDelegate methods
-(void)connection:(NSURLConnection *)connection didSendBodyData:(NSInteger)bytesWritten totalBytesWritten:(NSInteger)totalBytesWritten totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite{
    //NSLog(@"did send %d bytes",bytesWritten);
    int writenLength = (int)bytesWritten ;
//    [_lock lock];
    dispatch_queue_t queue = dispatch_queue_create("zenny_chen_firstQueue", nil);
    dispatch_async(queue, ^(void) {
        dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
        
        self.length += writenLength;
        self.endTime = [NSDate date];
        self.interval_t += [self.endTime timeIntervalSinceDate:self.startTime];
        
        //NSLog(@"length:%d, time:%f",[data length],[self.endTime timeIntervalSinceDate:self.startTime]);
        
        self.startTime = self.endTime;
        
        if (self.interval_t> self.caculateInterval && self.interval_t < 1.5) {
            float speed = (float)self.length / 1000 / 1000 * 8 / self.interval_t;
            NSLog(@"------->send:%10lu time:%f   >>%.2f", (unsigned long)self.length, self.interval_t, speed);
            self.length = 0;
            self.interval_t = 0;
            //[self.delegate currentUploadSpeed:speed completion:false];
            [self.delegate speedtest:self uploadSpeed:speed completion:false];
            
            if (speed > _maxUploadSpeed) {
                _maxUploadSpeed = speed;
            }
            caculateTotal += speed;
            caculateCount ++;
        }else if(self.interval_t >= 1.5){
            self.length = 0;
            self.interval_t = 0;
        }
        
        dispatch_semaphore_signal(sem);
    });
//    [_lock unlock];

}
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    self.startTime = [NSDate date];
//    NSLog(@"Thread:%@---Connection:%@",[NSThread currentThread],[connection description]);

//    NSLog(@"%@",[connection description]);
//    NSLog(@"%@",[NSThread currentThread]);
    
    for (int i = 0 ; i < self.numOfThread; i++) {
        if(connection == [self.dlConnections objectAtIndex:i]){
            shouldDownLoadLength[i] = [response expectedContentLength];
        }
    }
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    @try {
        for (int i = 0 ; i < self.numOfThread; i++) {
            if(connection == [self.dlConnections objectAtIndex:i]){
                dlFinish[i] = true;
            }
        }
        for (int i = 0 ; i < self.numOfThread; i++) {
            if(connection == [self.ulConnections objectAtIndex:i]){
                ulFinish[i] = true;
            }
        }
    }
    @catch (NSException *exception) {
        NSLog([exception description]);
    }
    @finally {
        
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    //    if(connection == [_connections objectAtIndex:0]){
    //        [_requestData appendData:data];
    //    }
    
    
//    NSLog(@"Thread:----%@",[NSThread currentThread]);
//    NSLog(@"did received %d bytes", [data length]);
    
    //NSLog([connection description]);
    //    dispatch_queue_t queue1 = dispatch_queue_create("com.dispatch.writedb", DISPATCH_QUEUE_SERIAL);
    //    dispatch_async(queue1, ^{
    //
    //    });
    /*
     if(connection == _connection1){
     _tr1didDownLoadLength+= [data length];
     if (_tr1didDownLoadLength >= _tr1DownLoadLength) {
     //_finished1 = YES;
     }
     }
     if(connection == _connection2){
     _tr2didDownLoadLength+= [data length];
     if (_tr2didDownLoadLength >= _tr2DownLoadLength) {
     _finished2 = YES;
     }
     }
     */
/*
    [_lock lock];
    //@synchronized(self.endTime){
    self.length += [data length];
    self.endTime = [NSDate date];
    self.interval_t += [self.endTime timeIntervalSinceDate:self.startTime];
    intervalTotal+= [self.endTime timeIntervalSinceDate:self.startTime];
    //NSLog(@"length:%d, time:%f",[data length],[self.endTime timeIntervalSinceDate:self.startTime]);
    
    self.startTime = self.endTime;
    
    if (self.interval_t>1 ) {
        NSLog(@"------->received:%.2f time:%f   >>%.2f", (float)self.length, self.interval_t, (float)self.length / 1024 / 1024 * 8 / self.interval_t);
        NSLog(@"totoal time:%f",intervalTotal);
        self.length = 0;
        self.interval_t = 0;
        self.lastUpdateDate = [NSDate date];
    }
    //}

    [_lock unlock];
*/
    
    dispatch_queue_t queue = dispatch_queue_create("zenny_chen_firstQueue", nil);
    dispatch_async(queue, ^(void) {
        dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
        self.length += [data length];
        self.endTime = [NSDate date];
        self.interval_t += [self.endTime timeIntervalSinceDate:self.startTime];
        intervalTotal += [self.endTime timeIntervalSinceDate:self.startTime];
        //NSLog(@"length:%d, time:%f",[data length],[self.endTime timeIntervalSinceDate:self.startTime]);
        
        self.startTime = self.endTime;
        
        //if (self.interval_t> self.caculateInterval && self.interval_t < 1.5 ) {
        if (self.interval_t> self.caculateInterval) {
            float speed = (float)self.length / 1024 / 1024 * 8 / self.interval_t;
            NSLog(@"------->received:%10lu time:%f   >>%.2f", (unsigned long)self.length, self.interval_t, speed);
            NSLog(@"totoal time:%f",intervalTotal);
            self.length = 0;
            self.interval_t = 0;
            self.lastUpdateDate = [NSDate date];
            //[self.delegate currentDownloadSpeed:speed completion:false];
            if (_isDownloading)
                [self.delegate speedtest:self downloadSpeed:speed completion:false];
            
            if (speed > _maxDownloadSpeed) {
                _maxDownloadSpeed = speed;
            }
            caculateTotal += speed;
            caculateCount ++;
        }
        /*
        else if ( self.interval_t >= 1.5){
            self.length = 0;
            self.interval_t = 0;
        }
        */

        dispatch_semaphore_signal(sem);
    });
}
#pragma mark - NSURLSessionDelegate methods
- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential *))completionHandler
{
    
    if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]){
        
        NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
        
        completionHandler(NSURLSessionAuthChallengeUseCredential,credential);
        
    }
    
}
@end
