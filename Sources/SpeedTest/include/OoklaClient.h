//
//  OoklaClient.h
//  test
//
//  Created by Kevin on 2015/2/17.
//  Copyright (c) 2015年 ZyXEL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OoklaClient : NSObject
@property NSString *ip;
@property NSString *lat;
@property NSString *lon;
@property NSString *isp;

@end
