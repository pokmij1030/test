//
//  OoklaServer.h
//  test
//
//  Created by Kevin on 2015/2/17.
//  Copyright (c) 2015年 ZyXEL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OoklaServer : NSObject
@property int index;
@property NSString *url;
@property NSString *url2;
@property NSString *lat;
@property NSString *lon;
@property NSString *name;
@property NSString *country;
@property NSString *country_code;
@property NSString *sponsor;
@property double distance;
@property double ping;
@end
