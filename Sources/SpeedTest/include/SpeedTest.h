//
//  SpeedTest.h
//  test
//
//  Created by Kevin on 2015/2/12.
//  Copyright (c) 2015年 ZyXEL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OoklaClient.h"
#import "OoklaServer.h"

enum {
    SpeedTestDownloadServerInfoError = 1000,
    SpeedTestDownloadClientInfoError,
    SpeedTestDownloadPaserServerInfoError,
    SpeedTestDownloadPaserClientInfoError,

};

enum SPEEDTESTWAY{
    SPEEDTESTWAY_ClinetToInternet = 0,
    SPEEDTESTWAY_ClientToGateway,
    SPEEDTESTWAY_GatewayToInternet
};
static NSString *const SpeedTestErrorDomain = @"com.zyxel.speedtest";

@class SpeedTest;
@protocol SpeedTestDelegate <NSObject>
@required
-(void)speedtest:(SpeedTest *)speedtest downloadSpeed:(float)speed completion:(Boolean)finished;
-(void)speedtest:(SpeedTest *)speedtest uploadSpeed:(float)speed completion:(Boolean)finished;
-(void)speedtest:(SpeedTest *)speedtest didFailWithError:(NSError *)error;
-(void)speedtest:(SpeedTest *)speedtest loading:(BOOL)flag;
@end

@interface SpeedTest : NSObject{
    int numOfThread;
    int numofCycle;
    int testImagePixel;
    int timeOut;
}

@property int numOfThread;
@property int numofCycle;
@property int testImagePixel;
@property int timeOut;
@property NSString *clientInfoPath;
@property NSString *serverInfoPath;
@property OoklaServer *manualSelectServer;
@property NSMutableArray *serverList;
@property Boolean autoSelectServerEnable;
@property float caculateInterval;
@property float maxDownloadSpeed;
@property float maxUploadSpeed;
@property enum SPEEDTESTWAY testWay;

@property (nonatomic, weak) id<SpeedTestDelegate> delegate;

-(void)downLoadClientInfo:(NSString *)path;
-(void)downLoadServerInfo:(NSString *)path;
-(void)paserServerInfo:(NSString *)clientInfoPath withClientInfo:(NSString *)serverInfoPath;
-(void)prepareForSpeedTest;
-(void)prepareForSpeedTest:(void(^)(void))completion;
-(NSArray*)getTop20ServerList;
-(void)setManualSelectServer:(OoklaServer*)server;
-(void)setNumOfThreads:(int)num;
-(void)setNumOfCycles:(int)num;
-(void)setTestImagePixel:(int)pixel;
//-(void)setSpeeTestWay:(SPEEDTESTWAY)way;
-(void)startDownloadTest;
-(void)startUploadTest;
@end
