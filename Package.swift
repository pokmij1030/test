// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "SpeedTestPackage",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(name: "SpeedTest", targets: ["SpeedTest"]),
        .library(name: "SpeedTestData", targets: ["SpeedTestData"]),
        .library(name: "CircularSlider", targets: ["CircularSlider"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        //.package(url: "https://github.com/jdg/MBProgressHUD.git",.exact("1.2.0")),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "SpeedTest",
            dependencies: [],
            publicHeadersPath: "include"  //Objective-C
        ),
        .target(
            name: "CircularSlider",  //swift
            dependencies: []
        ),
        .target(
            name: "SpeedTestData",
            dependencies: [],
            publicHeadersPath: "include" //Objective-C
        ),
    ]
)
